const path = require('path');
const fs = require('fs');
const solc = require('solc');

const contractPath = path.resolve(__dirname, 'contracts', 'HODLContract.sol')
const source = fs.readFileSync(contractPath, 'utf8');

var input = {
    language: 'Solidity',
    sources: {
        'HODLContract.sol': {
            content: source
        }
    },
    settings: {
        outputSelection: {
            '*': {
                '*': ['*']
            }
        }
    }
};

const compiled = JSON.parse(solc.compile(JSON.stringify(input)));
const contract = compiled.contracts['HODLContract.sol']['HODLContract'];

module.exports = contract;

