// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.6;

/**
 * @title HODLContract
 * @dev Lock away value until a given date/time.
 */
contract HODLContract
{
    // HODL object definition.
    struct HODL { uint value; uint32 expiration; }

    // HODL object mapping for each address.
    mapping (address => HODL) hodls;

    // Event to emit on deposits.
    event Deposit(address hodler, uint value, uint expiration);

    // Event to emit on withdrawals.
    event Withdrawal(address hodler);

    /**
     * @dev Check status of HODL for an address.
     * @return value uint
     * @return expiration uint32
     * @return eta uint32
     */
    function status() external view returns (uint value, uint32 expiration, uint32 eta)
    {
        // Get HODL for sender.
        HODL storage hodl = hodls[msg.sender];

        // Calculate ETA (time until expiration).
        eta = hodl.expiration > uint(block.timestamp) ? hodl.expiration - uint32(block.timestamp) : 0;

        // Return HODL details.
        return (hodl.value, hodl.expiration, eta);
    }

    /**
     * @dev Lock away value by creating a HODL object.
     * @param until Unix timestamp of date until which value is locked.
     */
    function deposit(uint32 until) external payable
    {
        // Get HODL for sender.
        HODL storage hodl = hodls[msg.sender];

        // Check that message contains non-zero value.
        require(msg.value > 0, "Message must contain a non-zero value.");

        // Check that address is not already HODLing.
        require(hodl.value == 0, "You're already HODLing.");

        // Check that expiration datetime is in the future.
        require(until > block.timestamp, "Value of 'until' must be a future UNIX timestamp.");

        // Set the HODL value.
        hodl.value = msg.value;

        // Set the HODL expiration.
        hodl.expiration = until;

        // Emit Deposit event.
        emit Deposit(msg.sender, hodl.value, hodl.expiration);
    }

    /**
     * @dev Transfer value to sender if HODL has expired.
     *
     * This method copies and resets the sender's HODL value before sending, using
     * a recommended pattern from the Solidity docs:
     * https://docs.soliditylang.org/en/v0.8.3/common-patterns.html#withdrawal-from-contracts
     */
    function withdraw() external
    {
        // Get HODL for sender.
        HODL storage hodl = hodls[msg.sender];

        // Check that sender address has value.
        require(hodl.value > 0, "No HODL in progress.");

        // Check that current time is on or after expiration.
        require(block.timestamp >= hodl.expiration, "HODL in progress. Be patient.");

        // Check contract address funds are sufficient to pay sender.
        require(address(this).balance >= hodl.value, "Insufficient contract balance.");

        // Get value of HODL.
        uint value = hodl.value;

        // Reset HODL value for sender.
        hodl.value = 0;

        // Release funds to HODLer.
        payable(msg.sender).transfer(value);

        // Emit Withdrawal event.
        emit Withdrawal(msg.sender);
    }
}

