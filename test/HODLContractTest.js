const assert = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const web3 = new Web3(ganache.provider());
const { abi, evm } = require('../compile');

let accounts;
let contract;

function until(sec = 60)
{
    return parseInt(Date.now() / 1000) + sec;
}

beforeEach(async () => {

    accounts = await web3.eth.getAccounts();

    contract = await new web3.eth.Contract(abi)
        .deploy({ data: evm.bytecode.object })
        .send({ from: accounts[0], gas: '1000000' });
});

describe('HODLContract', () => {

    it('is deployed', () => {

        assert.ok(contract.options.address);
    });

    it('can check initial status', async () => {
        
        const status = await contract.methods.status()
            .call({ from: accounts[0] });
            
        assert.equal(status.value, 0);
        assert.equal(status.expiration, 0);
    });

    it("can deposit value", async () => {

        await contract.methods.deposit(until())
            .send({ from: accounts[0], value: 1 });

        const status = await contract.methods.status()
            .call({ from: accounts[0] });

        assert.equal(status.value, 1);
    });

    it("cannot deposit without value", async () => {

        try
        {
            await contract.methods.deposit(until())
                .send({ from: accounts[0], value: 0});
            assert(false);
        }
        catch (err)
        {
            const tx = Object.keys(err.results)[0];
            const reason = 'Message must contain a non-zero value.';

            assert.equal(err.results[tx].error, 'revert');
            assert.equal(err.results[tx].reason, reason);
        }
    });

    it("cannot deposit consecutively", async () => {

        await contract.methods.deposit(until())
            .send({ from: accounts[0], value: 1});

        try
        {
            await contract.methods.deposit(until())
                .send({ from: accounts[0], value: 1});
            assert(false);
        }
        catch (err)
        {
            const tx = Object.keys(err.results)[0];
            const reason = "You're already HODLing.";

            assert.equal(err.results[tx].error, 'revert');
            assert.equal(err.results[tx].reason, reason);
        }
    });

    it("cannot deposit with a past expiration date", async () => {

        try
        {
            await contract.methods.deposit(until(-60))
                .send({ from: accounts[0], value: 1});
            assert(false);
        }
        catch (err)
        {
            const tx = Object.keys(err.results)[0];
            const reason = "Value of 'until' must be a future UNIX timestamp.";

            assert.equal(err.results[tx].error, 'revert');
            assert.equal(err.results[tx].reason, reason);
        }
    });

    it("cannot withdraw empty value", async () => {

        try
        {
            await contract.methods.withdraw()
                .send({ from: accounts[0] });
            assert(false);
        }
        catch (err)
        {
            const tx = Object.keys(err.results)[0];
            const reason = "No HODL in progress.";

            assert.equal(err.results[tx].error, 'revert');
            assert.equal(err.results[tx].reason, reason);
        }
    });

    it("cannot withdraw before expiration", async () => {

        await contract.methods.deposit(until())
            .send({ from: accounts[0], value: 1});

        try
        {
            await contract.methods.withdraw()
                .send({ from: accounts[0] });
            assert(false);
        }
        catch (err)
        {
            const tx = Object.keys(err.results)[0];
            const reason = "HODL in progress. Be patient.";

            assert.equal(err.results[tx].error, 'revert');
            assert.equal(err.results[tx].reason, reason);
        }
    });
});

