## 💎 Diamond Hands HODL

*An Ethereum Smart Contract for HODLers with Zero Risk Aversion -- That's right!*

Serious about HODLing long-term?

This contract allows you to lock your ether away and retrieve it no sooner than a future date of your choosing.

### Demo

A demo is available [here](https://garylocke.gitlab.io/diamond-hands-hodl/).

The demo contract is deployed on the Ropsten test network.

### Install

    git clone https://gitlab.com/garylocke/diamond-hands-hodl.git
    cd hodl-smart-contract && npm install

### Test

**Note:** The command below will automatically compile the contract, then deploy it locally using [Ganache](https://www.trufflesuite.com/ganache).

    npm run test

### Contribute

Pull requests are welcome. For major changes, open an issue first to discuss what you would like to change.

Be sure to update tests as appropriate.

### License
[GPLv3](https://choosealicense.com/licenses/gpl-3.0/)
